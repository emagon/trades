﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kenbi.Entities
{
    public class Trade
    {
        public Trade()
        {
            Created = DateTime.Now;
            Modified = Created;
        }

        [Key]       
        public Guid Id { get; set; }
        [Required]
        public DateTime Created { get; set; }        
        public DateTime Modified { get; set; }       
        public Guid? UserExecutorId { get; set; }

        [ForeignKey("UserExecutorId")]        
        public User UserExecutor { get; set; }
        public Guid PortfolioId { get; set; }
        [ForeignKey("PortfolioId")]
        [InverseProperty("Trades")]
        public Portfolio Portfolio { get; set; }
        public DateTime Date { get; set; }

        [Column(TypeName = "decimal(18,3)")]
        public decimal NumberOfShares { get; set; }

        [Column(TypeName = "decimal(18,3)")]
        public decimal Price { get; set; }        
        public string Currency { get; set; }

        [Column(TypeName = "decimal(18,3)")]
        public decimal MarketValue { get; set; }
        public string Action { get; set; }
        public string Notes { get; set; }
        public string Asset { get; set; }
    }
}
