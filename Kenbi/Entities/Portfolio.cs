﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Kenbi.Entities
{
    public class Portfolio
    {
        public Portfolio()
        {
            Created = DateTime.Now;
            Modified = Created;            
        }

        [Key]
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }               
        public string Name { get; set; }
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        [InverseProperty("Portfolios")]
        public User Owner { get; set; }        
        public virtual ICollection<Trade> Trades { get; set; }       
    }
}
