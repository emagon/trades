﻿using Kenbi.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kenbi.Migrations
{
    public class KenbiDbContext : DbContext
    {
        public KenbiDbContext(DbContextOptions<KenbiDbContext> options)  : base(options)
        {

        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Portfolio> Portfolios { get; set; }
        public virtual DbSet<Trade> Trades { get; set; }
    }
}
