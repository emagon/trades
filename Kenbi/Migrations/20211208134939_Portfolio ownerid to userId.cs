﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Trades.Migrations
{
    public partial class PortfolioowneridtouserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Portfolios_Users_OwnerId",
                table: "Portfolios");

            migrationBuilder.DropIndex(
                name: "IX_Portfolios_OwnerId",
                table: "Portfolios");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "Portfolios");

            migrationBuilder.RenameColumn(
                name: "User",
                table: "Portfolios",
                newName: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Portfolios_UserId",
                table: "Portfolios",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Portfolios_Users_UserId",
                table: "Portfolios",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Portfolios_Users_UserId",
                table: "Portfolios");

            migrationBuilder.DropIndex(
                name: "IX_Portfolios_UserId",
                table: "Portfolios");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Portfolios",
                newName: "User");

            migrationBuilder.AddColumn<Guid>(
                name: "OwnerId",
                table: "Portfolios",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_Portfolios_OwnerId",
                table: "Portfolios",
                column: "OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Portfolios_Users_OwnerId",
                table: "Portfolios",
                column: "OwnerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
