﻿using Kenbi.Entities;
using Kenbi.Helpers;
using Kenbi.Migrations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kenbi.Services
{    

    public interface IPortfolioService
    {
        Portfolio AddPortfolio(Portfolio portfolio);
        bool? DeletePortfolio(Guid Id);
        IEnumerable<Portfolio> ListPortfolios(Guid userId);
        decimal PortfolioBalance(Guid id);
    }

    public class PortfolioService : IPortfolioService
    {
        private KenbiDbContext _context;

        public PortfolioService(KenbiDbContext context)
        {
            _context = context;
        }

        public Portfolio AddPortfolio(Portfolio portfolio)
        {
            //verify if portfolio already exists
            var portfolioExist = _context.Portfolios.AsNoTracking().Any(x => x.UserId == portfolio.UserId && x.Name == portfolio.Name);

            if (portfolioExist)
                throw new KenbiException("Portfolio already exists");

            _context.Portfolios.Add(portfolio);
            _context.SaveChanges();

            return portfolio;
        }

        public bool? DeletePortfolio(Guid id)
        {
            var portfolio = _context.Portfolios.Find(id);

            //verify if portfolio already exists           
            if (portfolio == null)
                throw new KenbiException("Portfolio not found");

            _context.Portfolios.Remove(portfolio);

            return _context.SaveChanges() > 0;
        }

        public IEnumerable<Portfolio> ListPortfolios(Guid userId)
        {
            return _context.Portfolios.AsNoTracking().Where(x => x.UserId == userId);
        }

        public decimal PortfolioBalance(Guid id)
        {
            var portfolio = _context.Portfolios.AsNoTracking().Include(x => x.Trades).FirstOrDefault(x => x.Id == id);

            //verify if portfolio already exists           
            if (portfolio == null)
                throw new KenbiException("Portfolio not found");

            var tradePurchased = portfolio.Trades.Where(x => x.Action == "buy").Sum(x => x.NumberOfShares);
            var tradeSold = portfolio.Trades.Where(x => x.Action == "sell").Sum(x => x.NumberOfShares);

            return (tradePurchased - tradeSold) * GetCurrentMarketPrice();
        }

        private decimal GetCurrentMarketPrice()
        {
            //demo 
            return 15.48M;
        }
    }
}