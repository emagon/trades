﻿using Kenbi.Entities;
using Kenbi.Helpers;
using Kenbi.Migrations;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace Kenbi.Services
{

    public interface IUserService
    {
        User Login(string username, string password);
        IEnumerable<User> ListUsers();
        public User GetById(Guid id);
        User SignUp(User user, string password);

        User RefreshToken(string token);
        bool RevokeToken(string token);
    }

    public class UserService : IUserService
    {
        private KenbiDbContext _context;
        private readonly AppSettings _appSettings;

        public UserService(KenbiDbContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        public User Login(string username, string password)
        {
            // Validate if username or password is valid
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = _context.Users.FirstOrDefault(x => x.Username == username);

            // check if user exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, user.Password, user.Salt)) return null;

            // authentication successful. Generate token
            GenerateToken(user);

            GenerateRefreshToken(user);

            return user;
        }

        public User SignUp(User user, string password)
        {
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new KenbiException("Password is required");

            if (_context.Users.Any(x => x.Username == user.Username))
                throw new KenbiException("Username \"" + user.Username + "\" is already taken");

            CreatePasswordHash(password, out string passwordHash, out string passwordSalt);

            user.Password = passwordHash;
            user.Salt = passwordSalt;

            _context.Users.Add(user);
            _context.SaveChanges();

            return user;
        }

        public User GetById(Guid id)
        {
            return _context.Users.Find(id);
        }

        public IEnumerable<User> ListUsers()
        {
            return _context.Users;
        }

        public User RefreshToken(string token)
        {
            var user = _context.Users.SingleOrDefault(u => u.RefreshToken == token);

            // return null if no user found with token
            if (user == null) return null;

            // return null if token is no longer active
            if (user.RefreshTokenExpiration <= DateTime.Now) return null;

            // replace old refresh token with a new one and save
            GenerateRefreshToken(user);

            // generate new jwt
            GenerateToken(user);

            return user;
        }

        public bool RevokeToken(string token)
        {
            var user = _context.Users.SingleOrDefault(u => u.RefreshToken == token);

            // return false if no user found with token
            if (user == null) return false;

            // return null if token is no longer active
            if (user.RefreshTokenExpiration <= DateTime.Now) return false;

            // revoke token and save
            user.RefreshToken = "";
            user.RefreshTokenExpiration = null;
            _context.Update(user);
            _context.SaveChanges();

            return true;
        }

        // helper methods


        /// <summary>
        /// Creates hash from password
        /// </summary>
        /// <param name="password"></param>
        /// <param name="passwordHash"></param>
        /// <param name="passwordSalt"></param>
        private static void CreatePasswordHash(string password, out string passwordHash, out string passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new KenbiException("Value cannot be empty or whitespace only string.", "password");

            using var hmac = new System.Security.Cryptography.HMACSHA512();
            passwordSalt = Convert.ToBase64String(hmac.Key);
            passwordHash = Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(password)));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <param name="storedHash"></param>
        /// <param name="storedSalt"></param>
        /// <returns>If password is correct or not</returns>
        private static bool VerifyPasswordHash(string password, string storedHash, string storedSalt)
        {
            var passwordBytes = Convert.FromBase64String(storedHash);
            var saltBytes = Convert.FromBase64String(storedSalt);

            if (password == null) throw new ArgumentNullException("password is null");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (passwordBytes.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (saltBytes.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(saltBytes))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordBytes[i]) return false;
                }
            }

            return true;
        }


        private User GenerateToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddMinutes(15),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);

            user.Token = tokenHandler.WriteToken(token);
            user.TokenExpiration = tokenDescriptor.Expires.Value;

            _context.SaveChanges();

            return user;
        }

        /// <summary>
        /// Generate RefreshToken
        /// </summary>
        /// <param name="user"></param>
        /// <returns>User</returns>
        private User GenerateRefreshToken(User user)
        {
            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var randomBytes = new byte[64];
            rngCryptoServiceProvider.GetBytes(randomBytes);

            //Set refreshtoken on user
            user.RefreshToken = Convert.ToBase64String(randomBytes);

            //set refresh token Expiration date
            user.RefreshTokenExpiration = DateTime.UtcNow.AddDays(7);

            _context.Update(user);
            _context.SaveChanges();

            return user;
        }

    }
}
