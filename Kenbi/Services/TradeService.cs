﻿using Kenbi.Entities;
using Kenbi.Helpers;
using Kenbi.Migrations;
using Kenbi.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kenbi.Services
{
    public interface ITradeService
    {
        Trade AddTrade(Trade trade);
        bool? DeleteTrade(Guid Id);
        IEnumerable<Trade> ListTrades(Guid portfolioId);
    }

    public class TradeService : ITradeService
    {
        private KenbiDbContext _context;

        public TradeService(KenbiDbContext context)
        {
            _context = context;
        }

        public Trade AddTrade(Trade trade)
        {
            //verify if any action
            if (string.IsNullOrWhiteSpace(trade.Action))
                 throw new KenbiException("Action cannot be empty");

            //verify if is sell ou buy
            if (trade.Action != "sell" && trade.Action != "buy")
                 throw new KenbiException("Wrong type of action");

            //Check if currency has value
            if (string.IsNullOrWhiteSpace(trade.Currency))
                 throw new KenbiException("Currency cannot be empty");

            //Check if currency has correct format
            if (trade.Currency.Length != 3)
                 throw new KenbiException("Wrong format of currency");

            //check if the user has enough shares to sell
            if (trade.Action == "sell")
                
                if (CanSellShare(trade.PortfolioId, trade.NumberOfShares) == false)
                    throw new KenbiException("Not enough shares to sell");          

            _context.Trades.Add(trade);
            _context.SaveChanges();

            return trade;
        }

        public bool? DeleteTrade(Guid id)
        {
            var trade = _context.Trades.Find(id);

            if (trade == null)
                throw new KenbiException("Trade not found"); ;

            _context.Trades.Remove(trade);

            return _context.SaveChanges() > 0;
        }

        public IEnumerable<Trade> ListTrades(Guid portfolioId)
        {
            return _context.Trades.AsNoTracking().Where(x => x.PortfolioId == portfolioId);
        }

        private bool? CanSellShare(Guid portfolioId, decimal share)
        {
            var portfolio = _context.Portfolios.AsNoTracking().Include(x => x.Trades).FirstOrDefault(x => x.Id == portfolioId);

            if (portfolio == null)
                return null;

            //verify 
            return calculateShare(portfolio) >= share;
            
        }

        private decimal calculateShare(Portfolio portfolio)
        {
            var tradePurchased = portfolio.Trades.Where(x => x.Action == "buy").Sum(x => x.NumberOfShares);
            var tradeSold = portfolio.Trades.Where(x => x.Action == "sell").Sum(x => x.NumberOfShares);

            return (tradePurchased - tradeSold);            
        }
    }
}
