﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace Kenbi.Helpers
{ 
    public class KenbiException : Exception
    {
        public KenbiException() : base() { }

        public KenbiException(string message) : base(message) { }

        public KenbiException(string message, params object[] args)
            : base(String.Format(CultureInfo.CurrentCulture, message, args))
        {
        }
    }
}
