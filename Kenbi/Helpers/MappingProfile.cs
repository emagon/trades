﻿using AutoMapper;
using Kenbi.Entities;
using Kenbi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kenbi.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserModel, User>();
            CreateMap<User, UserModel>();

            CreateMap<SignUpModel, User>()
                .ForMember(x => x.Username, d => d.MapFrom(s => s.Username))
                .ForMember(x => x.Name, d => d.MapFrom(s => s.Name))
                .ForMember(x => x.Email, d => d.MapFrom(s => s.Email));

            CreateMap<PortfolioModel, Portfolio>();
            CreateMap<Portfolio, PortfolioModel>();

            CreateMap<Trade, TradeModel>()
                .ForMember(x => x.User_Executor, d => d.MapFrom(s => s.UserExecutorId))
                .ForMember(x => x.Portfolio, d => d.MapFrom(s => s.PortfolioId))
                .ForMember(x => x.Number_Of_Shares, d => d.MapFrom(s => s.NumberOfShares))
                .ForMember(x => x.Market_Value, d => d.MapFrom(s => s.MarketValue));

            CreateMap<TradeModel, Trade>()
                .ForMember(x => x.UserExecutorId, d => d.MapFrom(s => s.User_Executor))
                .ForMember(x => x.PortfolioId, d => d.MapFrom(s => s.Portfolio))
                .ForMember(x => x.Portfolio, d => d.Ignore())
                .ForMember(x => x.NumberOfShares, d => d.MapFrom(s => s.Number_Of_Shares))
                .ForMember(x => x.MarketValue, d => d.MapFrom(s => s.Market_Value))
                .ForMember(x => x.Created, d => d.Ignore())
                .ForMember(x => x.Modified, d => d.Ignore());
        }
    }
}
