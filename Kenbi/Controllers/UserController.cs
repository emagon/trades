﻿using AutoMapper;
using Kenbi.Entities;
using Kenbi.Helpers;
using Kenbi.Models;
using Kenbi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Kenbi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;

        public UserController(
            IUserService userService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _mapper = mapper;
        }


        [AllowAnonymous]
        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.Login(model.UserName, model.Password);

                if (user == null)
                    return Unauthorized(new { message = "Username or password is incorrect" });

                // return user info and authentication token
                return Ok(_mapper.Map<UserModel>(user));
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [AllowAnonymous]
        [HttpPost("signup")]
        public IActionResult Signup([FromBody] SignUpModel model)
        {
            if (ModelState.IsValid)
            {
                // map model to entity
                var user = _mapper.Map<SignUpModel, User>(model);

                try
                {
                    // create user
                    _userService.SignUp(user, model.Password);

                    return Ok("User created successfully");
                }
                catch (KenbiException ex)
                {
                    // return error message if there was an exception
                    return BadRequest(new { message = ex.Message });
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [AllowAnonymous]
        [HttpPost("refreshtoken")]
        public IActionResult Signup([FromBody] string token)
        {
            // validate token
            var user = _userService.RefreshToken(token);

            if (user == null)
                return BadRequest(new { message = "Invalid token" });

            return Ok(_mapper.Map<UserModel>(user));

        }

        [HttpPost("revoketoken")]
        public IActionResult RevokeToken([FromBody] string token)
        {
            // accept token from request body or cookie
            token ??= Request.Cookies["refreshToken"];

            if (string.IsNullOrEmpty(token))
                return BadRequest(new { message = "Token is required" });

            var response = _userService.RevokeToken(token);

            if (!response)
                return NotFound(new { message = "Token not found" });

            return Ok(new { message = "Token revoked" });
        }

        [Authorize]
        [HttpGet("ListUsers")]
        public IActionResult ListUsers()
        {
            var users = _userService.ListUsers();

            var models = new List<UserModel>();

            users.ToList().ForEach(x => models.Add(_mapper.Map<UserModel>(x)));

            return Ok(models);
        }
    }
}
