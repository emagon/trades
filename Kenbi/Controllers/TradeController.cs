﻿using AutoMapper;
using Kenbi.Entities;
using Kenbi.Helpers;
using Kenbi.Models;
using Kenbi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kenbi.Controllers
{    
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TradeController : ControllerBase
    {
        private ITradeService _tradeService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public TradeController(
           ITradeService tradeService,
           IMapper mapper,
           IOptions<AppSettings> appSettings)
        {
            _tradeService = tradeService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        // GET: api/<TradeController>/ListTrades/202b8104-caa1-4c01-2be6-08d9b996872b
        [HttpGet("ListTrades/{portfolioId}")]
        public IActionResult ListTrades(Guid portfolioId)
        {
            var trades = _tradeService.ListTrades(portfolioId);

            return Ok(trades);
        }

        // POST api/<TradeController>/AddTrade
        [HttpPost("AddTrade")]
        public IActionResult AddTrade([FromBody] TradeModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var trade = _tradeService.AddTrade(_mapper.Map<Trade>(model));

                    return Ok(_mapper.Map<TradeModel>(trade));
                }
                catch (KenbiException ex)
                {
                    // return error message if there was an exception
                    return BadRequest(new { message = ex.Message });
                }

            }
            else
            {
                return BadRequest(ModelState);
            } 
        }

        // DELETE api/<TradeController>/Delete/202b8104-caa1-4c01-2be6-08d9b996872b
        [HttpDelete("Delete/{tradeId}")]
        public IActionResult DeleteTrade(Guid tradeId)
        {
            try
            {
                var res = _tradeService.DeleteTrade(tradeId);
                
                if (res == false)
                    return BadRequest("Could not delete trade.");

                return Ok();
            }
            catch (KenbiException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }            
        }
    }
}
