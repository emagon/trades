﻿using AutoMapper;
using Kenbi.Entities;
using Kenbi.Helpers;
using Kenbi.Models;
using Kenbi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Kenbi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PortfolioController : ControllerBase
    {
        private IUserService _userService;
        private IPortfolioService _porfolioService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public PortfolioController(
            IUserService userService,
           IPortfolioService porfolioService,
           IMapper mapper,
           IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _porfolioService = porfolioService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        // GET: api/<PortfolioController>/ListPortfolios/202b8104-caa1-4c01-2be6-08d9b996872b
        [HttpGet("ListPortfolios/{userId}")]
        public IActionResult ListPortfolios(Guid userId)
        {
            var user = _userService.GetById(userId);

            if (user == null)
                return BadRequest("Invalid user");

            var portfolios = _porfolioService.ListPortfolios(userId);

            var models = _mapper.Map<List<PortfolioModel>>(portfolios);

            return Ok(models);
        }

        // POST api/<PortfolioController>/AddPortfolio
        [HttpPost("AddPortfolio")]
        public IActionResult AddPortfolio([FromBody] PortfolioModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.GetById(model.UserId);

                try
                {
                    if (user == null)
                        return BadRequest("Invalid user");

                    if (string.IsNullOrWhiteSpace(model.Name))
                        return BadRequest("Name connot be empty");

                    var portfolio = _porfolioService.AddPortfolio(_mapper.Map<Portfolio>(model));

                    return Ok(portfolio);
                }
                catch (KenbiException ex)
                {
                    // return error message if there was an exception
                    return BadRequest(new { message = ex.Message });
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        // DELETE api/<PortfolioController>/Delete/fef033d7-b77a-4a10-9855-c49cf47c1f37
        [HttpDelete("Delete/{id}")]
        public IActionResult DeletePortfolio(Guid id)
        {
            try
            {
                var res = _porfolioService.DeletePortfolio(id);

                if (res == false)
                    return BadRequest("Could not delete portfolio");

                return Ok();
            }
            catch (KenbiException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        // Get api/<PortfolioController>/Balance/fef033d7-b77a-4a10-9855-c49cf47c1f37
        [HttpGet("Balance/{id}")]
        public IActionResult PortfolioBalance(Guid id)
        {
            try
            {
                var res = _porfolioService.PortfolioBalance(id);                

                return Ok(res);
            }
            catch (KenbiException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
