﻿using System;

namespace Kenbi.Models
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Salt { get; set; }
        public string Token { get; set; }
        public string Refresh_Token { get; set; }
        public DateTime Token_Expiration { get; set; }
        public DateTime Refresh_Token_Expiration { get; set; }
    }
}
