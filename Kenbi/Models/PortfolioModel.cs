﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kenbi.Models
{
    public class PortfolioModel
    {
        public Guid Id { get; set; }

        [Required]
        public Guid UserId { get; set; }

        [Required]
        public string Name { get; set; }
    }
}