﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kenbi.Models
{
    public class TradeModel
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public DateTime Created { get; set; }
        [Required]
        public DateTime Modified { get; set; }
        [Required]
        public Guid User_Executor { get; set; }
        [Required]
        public Guid Portfolio { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public decimal Number_Of_Shares { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public string Currency { get; set; }
        [Required]
        public decimal Market_Value { get; set; }
        [Required]
        public string Action { get; set; }        
        public string Notes { get; set; }
        [Required]
        public string Asset { get; set; }
    }
}
